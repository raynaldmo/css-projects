window.onload = function() {
  // Open mobile menu
  var toggleButton = document.querySelector('.toggle-button');
  var mobileNav = document.querySelector('.mobile-nav');
  var closeButton = document.querySelector('.mobile-nav__close-button');

  mobileNav.style.display = 'none';

  toggleButton.addEventListener('click', function () {
    // console.log('button-clicked');

    // console.log(mobileNav.style.display);

    mobileNav.style.display =
        mobileNav.style.display === 'none' ? 'block' : 'none';
    // console.log(mobileNav.style.display);
  });

  // Close mobile menu
  closeButton.addEventListener('click', function () {
    mobileNav.style.display = 'none';
  });

  // Modal for homepage
  var modal = document.querySelector('.modal');

  if (modal) {
    var backdrop = document.querySelector('.backdrop');

    var selectPlanButtons = document.querySelectorAll('.button');

    // Open modal - version 1
    /*
    selectPlanButtons.forEach(function (button) {
      button.addEventListener('click', function () {
        backdrop.style.display = 'block';
        modal.style.display = 'block';
      });
    });
    */

    // Open modal - version 2
    for (var i = 0; i < selectPlanButtons.length; i++) {
      selectPlanButtons[i].onclick = function () {
        backdrop.style.display = 'block';
        modal.style.display = 'block';
      };
    }

    // Close modal
    var modal_action_no_btn = document.querySelector('.modal__action--negative');
    modal_action_no_btn.addEventListener('click', function () {
      backdrop.style.display = 'none';
      modal.style.display = 'none';
    });
  }

};



