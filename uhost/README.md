## CSS - The Complete Guide (incl. Flexbox, Grid and Sass) Video Course

### Installing and Running
```
# Install libraries via composer
cd /Users/Raynald/PhpstormProjects/css-projects/uhost
composer install

# Launch PHP built-in server
cd /Users/Raynald/PhpstormProjects/css-projects
php -S localhost:8000

# Launch site from browser
http://localhost:8000/uhost/index.php

```
### Chapter 1: Getting Started
* This project builds a fictional site called uHost

#### Chapter 2: Diving Into the Basics of CSS

* Add Anton font from Google Fonts
* Go to https://fonts.google.com
    * Search for Anton font
* Add the following to ```<head>``` in index.hml  
```<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">```

#### Chapter 3: Diving Deeper into CSS
##### Understanding Box Sizing
  * Best practice to use ``box-sizing: border-box`` for all elements
  ```css
  * {
   box-sizing: border-box;
  }
  ```
  
##### Add Header and Navigation Links
* Use **_BEM methodology_** (https://en.bem.info/methodology/) for class naming
* Use BEM to name classes for  main navigation ```nav ul li``` elements
* Use ``` display: inline-block, text-align: right``` and ```width``` properties
to position navigation links
* Use ```vertical-align``` property to vertically align inline elements
* Style "Start Hosting" nav link as CTA button

##### Add background image to Product Overview Section
* ```background-image: url("../images/freedom.jpg")```
* Image needs some additional styling - this will be done later


#### Chapter 4: More on Selectors and CSS Features
* Using ```!important``` on a property is bad practice and should only be used
  in edge cases
* Use ```:not()``` on a rule to **_not_** select a certain element

#### Chapter 5: Practicing the Basics
* Add HTML for ```plans``` section
* Style ```plans``` section
  * Use BEM methodology to name classes
* Use ```display: inline-block``` to put all plans in a single row
  * _This will break on small screens and will be fixed in responsive design section_
* Note at this point the height of the plan boxes are not the same.
  * Later on ```flexbox``` will be used to fix this
* Use ```font: inherit``` to style button to override font related browser defaults for button 
* Use the following CSS to get rid of blue outline on button when button is clicked
```
.button:focus {
  outline: none;
}
``` 
* Add HTML for ```key-features``` section
* Style ```key-features``` section
  * Add placeholder circles for images for now
* Use ```text-align: center``` on a container element to center all child elements
* Use ```vertical-align: top``` on a container element to control vertical alignment of child elements
* Add HTML and style ```main-foooter``` section
* Add ```packages/index.html``` page
* Re-factor css files to ```shared.css```, ```main.css``` and ```packages.css```
* Style ```packages/index.html``` page

#### Chapter 6: Positioning Elements with CSS
* Implement fixed navigation bar using ```position: fixed```
```
.main-header {
  background-color: #2ddf5c;
  padding: 0.5em 1em;
  position: fixed;
  width: 100%;
  top: 0;
  left: 0
}
```

* Add "RECOMMENDED" badge to "Plus Plan" using absolute positioning

#### Chapter 7: Understanding Background Images and Images
* Use ```background``` properties to style home page background image
* Add and style logo image
* Add customers page and customer images with ```<img>``` tag
* Style customer images using ```display-inline and vertical-align```
* Discuss 
  * Linear and Radial Gradients
  * Stacking Multiple Backgrounds
  * Filters
  * SVGs
* Use fontawesome icons instead of SVGs for home page key features icons

#### Chapter 8: Sizes and Units
* Add ```backdrop div``` to all pages
  * Demonstrates methods to make ```height: 100%``` setting work on an element
* Change ```font-size```, ```width```, ```height```, ```padding```, ```margin``` units to ```rem``` 
* Use ```vw``` and ```vh``` units for home page hero image (```#product-overview```) width/height

#### Chapter 9: Working with JavaScript & CSS
* Add shared.js file
* Open modal when user clicks on "Choose Plans" button on homepage
* In preparation for making the site responsive add mobile menu
  * Add click-able hamburger menu button 
* **_NOTE: Pause here to refactor code with TWIG_**

#### Chapter 10: Making our Website Responsive
* Added "scroll to top" icon to footer
* Made navigation and all pages responsive

#### Chapter 11: Adding and Styling Forms
* Styling Inputs & Buttons
* Validation Feedback Styles
* Styled sign up form in an alternate fashion from video
  * See ```start-hosting-alternate.twig``` and ```start-hosting-alternate.css```
* Style sign up form as per video
* Customize the look of the checkbox
  * Key is to set ```appearance: none```
```
/* Customize the look of the checkbox */
.signup-form input[type='checkbox'] {
  border: 1px solid #ccc;
  background-color: white;
  width: 1rem;
  height: 1rem;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}

/* Customize the look of the checkbox when checked */
.signup-form input[type='checkbox']:checked {
  background-color: #2ddf5c;
  border-color: #0e4f1f;
}
```
* Form validation
  * Robust form validation should be done at server - client can be  
hacked and fooled
  * Can still do basic validation and validation feedback at client though

#### Chapter 12: Working with Text and Fonts
* Remove link to Google fonts in layout.twig
* Add Google fonts using ```@import``` in ```shared.css```
* Add example ```@font-face``` rules in packages.css

#### Chapter 13: Adding Flexbox
