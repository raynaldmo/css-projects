* Fictional site for **Franklin Running Club**
* This project is from Safari Online Book CSS in Depth
(https://learning.oreilly.com/library/view/css-in-depth/9781617293450/kindle_split_007.html)
Part 2
* Main CSS topics covered in Part 2 of the book
  * Floats
  * Flexbox
  * Grid layout
* _TODO_  - Make these additional modifications
  * Main and mobile menu
  * Style switcher
  * Responsive design
