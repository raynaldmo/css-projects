window.onload = function() {
    let link = document.getElementsByTagName('link')[0];
    let button = document.querySelector('.button');

    setStyleName(link);
    button.onclick = function btnHandler () {
        if (link.href.indexOf('css/none.css') !== -1) {
            link.href = 'css/float.css';
        } else if (link.href.indexOf('css/float.css') !== -1) {
            link.href = 'css/flexbox.css';
        } else if (link.href.indexOf('css/flexbox.css') !== -1) {
            link.href = 'css/flexbox1.css'
        } else if (link.href.indexOf('css/flexbox1.css') !== -1) {
            link.href = 'css/none.css'
        }

        // console.log(link.href);
        setStyleName(link);
    }
    // console.log(link.href);

    function setStyleName(link) {
        linkArr = link.href.split('/');
        styleName = linkArr[linkArr.length - 1]
        button.innerHTML = "Switch Styles" + " (" + styleName + ") ";
    }
};

