## Bootstrap 4 Examples (with SASS and Gulp)
## References
### Bootstrap
* Packt - Bootstrap 4 Development for Professionals Video Course
### Gulp
* https://coder-coder.com/gulp-4-walk-through/
* https://medium.com/swlh/setting-up-gulp-4-0-2-for-bootstrap-sass-and-browsersync-7917f5f5d2c5
* https://www.sitepoint.com/introduction-gulp-js/
  
### Steps
* Check that ```nvm``` is installed
* Check that ```node.js``` is installed
* Check that gulp-cli is installed globally. If not install
 * ```npm list -g --depth=0```
 * ```npm install gulp-cli -g```
* Create ```package.json``` file **_first_**
  * ```npm init --yes```
* Install bootstrap
  * ```npm install bootstrap@4.6.0 jquery popper.js --save```
* Install fontawesome
  * ```npm install --save @fortawesome/fontawesome-free```
* Install gulp, gulp-sass etc.
  * ```npm install --save-dev autoprefixer cssnano gulp-postcss gulp-replace gulp-sourcemaps gulp-uglify```
* Create gulfile.js