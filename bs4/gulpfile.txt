// Gulp.js configuration

// modules
const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
// const minifycss = require('gulp-minify-css');
// const rename = require('gulp-rename');
// const uglify = require('gulp-uglify-es').default;


// compile scss files
function style() {
  return gulp.src('src/scss/**/*.scss')
      .pipe(sass().on('error',sass.logError))
      .pipe(gulp.dest('web/css'))
}

function scripts() {
  return gulp.src([
    /* Add your JS files here, they will be combined in this order */
    'node_modules/jquery/dist/jquery.js',
    'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
    'src/js/main.js',
//    'src/js/other.js'
    ])
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('web/js'))
//   .pipe(rename({suffix: '.min'}))
//   .pipe(uglify())
//   .pipe(gulp.dest('./js'));
}

function watch() {
  gulp.watch("src/js/*.js", scripts);
  gulp.watch('src/scss/**/*.scss', style);
}

exports.style = style;
exports.scripts = scripts;
exports.watch = watch;