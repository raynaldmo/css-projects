// Gulp.js configuration

// modules
const gulp = require('gulp');
const newer = require('gulp-newer');
const imagemin = require('gulp-imagemin');

const sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    assets = require('postcss-assets'),
    autoprefixer = require('autoprefixer'),
    mqpacker = require('css-mqpacker'),
    cssnano = require('cssnano');

// development mode?
const devBuild = (process.env.NODE_ENV !== 'production');

// folders
const src = 'src/', build = 'web/';

// image processing
function images() {
  const out = build + 'img/';

  return gulp.src(src + 'img/**/*')
      .pipe(newer(out))
      .pipe(imagemin({ optimizationLevel: 5 }))
      .pipe(gulp.dest(out));

}
exports.images = images;

// CSS processing
function css() {

  return gulp.src(src + 'scss/styles.scss')
      // .pipe(sourcemaps ? sourcemaps.init() : noop())
      .pipe(sass({
        outputStyle: 'nested',
        imagePath: '/img/',
        precision: 3,
        errLogToConsole: true
      }).on('error', sass.logError))
      // .pipe(postcss([
      //   assets({ loadPaths: ['img/'] }),
      //   autoprefixer({ browsers: ['last 2 versions', '> 2%'] }),
      //   mqpacker,
      //   cssnano
      // ]))
      // .pipe(sourcemaps ? sourcemaps.write() : noop())
      .pipe(gulp.dest(build + 'css/'));

}
exports.css = gulp.series(images, css);
