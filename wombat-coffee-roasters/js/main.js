// Add and remove 'is-open' class from menu container
(function () {
  let button = document.getElementById('toggle-menu');
  button.addEventListener('click', function (evt) {
    evt.preventDefault();
    let menu = document.getElementById('main-menu');
    menu.classList.toggle('is-open');
  })
})();